## Hookers  
An allianceauth plugin to push events through a discord webhook.

### Installation
1. Activate your AllianceAuth venv.
2. Install the application via pip: `$ python3 -m pip install git+https://gitlab.com/colcrunch/hookers.git`
3. Add `hookers` to your `INSTALLED_APPS` in `local.py`
4. Add the settings listed in the settings section to the end of `local.py`
5. Run the migrations, and restart your workers.
6. Add the check models tasks that you need via the periodic tasks page on the admin site.

#### Settings
```python
#######################################
#          HOOKER SETTINGS            #
#######################################
MAIN_AUTH_URL = ""
AUTHOR_ICON_URL = ""
AUTHOR_NAME = ""
CREATION_AND_UPDATES = {
    "timerboard.timer": False,
    "optimer.timer": False,
    "moonstuff.extraction": False,
}
```
**Note: If you do not set `MAIN_AUTH_URL` all URLs in embeds will go to https://eveonline.com/**

**Note: If you do not set `AUTHOR_ICON_URL` it will be set to the alliance auth logo.**

## Tasks
All check tasks take the following arguments:
* `preping` Boolean - Set to true if you want the specified object type to be prepinged 30 minutes out. (They will still be pinged at the actual event time)
* `hours`, `minutes`, `seconds` All Integer - Should be 1 second less than the frequency at which you want the task to run to avoid duplicates.
  * Ex. If `check_timerboard_models` runs every 6 hours, then `hours` should be 5, `minutes` should be 59, and `seconds` should also be 59
  
## Webhooks
This plugin will send pings through as many webhooks as you can configure, which makes it perfect for
sending pings to multiple discords (such as corp discords).

When you add a webhook you must tie it to a user. If that user's state ever changes, the webhook will autobatically
be removed to ensure that you aren't sending intel to former corps/na'er-do-wells.

## Colors
The color object exists to allow you to set the color of embeds on a per-type basis. 

For all types, the color set for `all` will be used as a fallback. If a color is not set for `all`, then
a yellow will be used by defualt.

**Note: Preping colors (color objects with the preping box ticked) are distinct from normal colors. You will need to create separate color
objects for both prepings, and normal pings (color objects with the preping box un-ticked)**