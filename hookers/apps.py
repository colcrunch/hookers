from django.apps import AppConfig


class HookersConfig(AppConfig):
    name = 'hookers'

    def ready(self):
        from . import signals