from django.forms import ModelForm
from django.forms.widgets import TextInput, NumberInput
from .models import Color


# Create your forms here.
class ColorForm(ModelForm):
    class Meta:
        model = Color
        fields = '__all__'
        widgets = {
            'color': TextInput(attrs={'type': 'color'})
        }
