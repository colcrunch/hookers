from django.contrib import admin
from .models import Webhook, Color
from .forms import ColorForm

# Register your models here.
admin.site.register(Webhook)


class ColorAdmin(admin.ModelAdmin):
    form = ColorForm


admin.site.register(Color, ColorAdmin)
