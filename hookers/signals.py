import logging
from django.conf import settings

from django.dispatch import receiver
from django.db.models.signals import post_save

from allianceauth.authentication.signals import state_changed
from allianceauth.authentication.models import UserProfile
from .models import Webhook

logger = logging.getLogger(__name__)


@receiver(state_changed, sender=UserProfile)
def member_state_changed(sender, user, state, **kwargs):
    logger.info("Received state_changed from %s to state %s" % (user, state))
    hooks = Webhook.objects.filter(user=user)
    for hook in hooks:
        hook.delete()


# Conditionally Load post_save signal recievers
if "allianceauth.timerboard" in settings.INSTALLED_APPS and settings.CREATION_AND_UPDATES['timerboard.timer']:
    from allianceauth.timerboard.models import Timer
    from .tasks import send_timer_event

    @receiver(post_save, sender=Timer)
    def timer_saved(sender, instance, created, *args, **kwargs):
        logger.info("Received post_save from %s (Created %s)" % (instance, created))
        if created:
            send_timer_event(instance.pk, title_post="**CREATED**")
        if 'update_fields' in kwargs:
            send_timer_event(instance.pk, title_post="**UPDATED**", desc_post=kwargs['update_fields'])


if "allianceauth.optimer" in settings.INSTALLED_APPS and settings.CREATION_AND_UPDATES['optimer.timer']:
    from allianceauth.optimer.models import OpTimer
    from .tasks import send_optimer_event

    @receiver(post_save, sender=OpTimer)
    def optimer_saved(sender, instance, created, *args, **kwargs):
        logger.info("Received post_save from %s (Created %s)" % (instance, created))
        if created:
            send_optimer_event(instance.pk, title_post="**CREATED**")
        if 'update_fields' in kwargs:
            send_optimer_event(instance.pk, title_post="**UPDATED**", desc_post=kwargs['update_fields'])


if "moonstuff" in settings.INSTALLED_APPS and settings.CREATION_AND_UPDATES['moonstuff.extraction']:
    from moonstuff.models import ExtractEvent
    from .tasks import send_extract_event

    @receiver(post_save, sender=ExtractEvent)
    def extraction_saved(sender, instance, created, *args, **kwargs):
        logger.info("Recieved post_save from %s (Created %s)" % (instance, created))
        if created:
            send_extract_event(instance.pk, title_post="**CREATED**")
        else:
            send_extract_event(instance.pk, title_post="**UPDATE**", desc_post=kwargs['update_fields'])
