from django.db import models
from allianceauth.authentication.models import User
from model_utils import Choices


# Create your models here.
class Webhook(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    hook = models.CharField(max_length=255, null=False)
    _type_enum = Choices("all", "timerboard.timer", "optimer.timer", "moonstuff.extraction")
    hook_type = models.CharField(max_length=50, choices=_type_enum)

    def __str__(self):
        return "{}'s {} Webhook".format(self.user, self.hook_type)


class Color(models.Model):
    _type_enum = Choices("all", "timerboard.timer", "optimer.timer", "moonstuff.extraction")
    hook_type = models.CharField(max_length=50, choices=_type_enum)
    color = models.TextField(max_length=20)
    preping = models.BooleanField(default=False)

    def __str__(self):
        return "{} Color (Preping: {})".format(self.hook_type, self.preping)

    class Meta:
        unique_together = (('preping', 'hook_type'),)
