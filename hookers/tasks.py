from celery import shared_task
from django.conf import settings
import logging
from datetime import datetime, timedelta
from .models import Webhook, Color
import requests
from django.urls import reverse


logger = logging.getLogger(__name__)


class EmbedSettings:

    @staticmethod
    def full_url(reversed_path):
        if settings.MAIN_AUTH_URL is "":
            url = "https://eveonline.com"
        else:
            url = "{}{}".format(settings.MAIN_AUTH_URL, reversed_path)
        return url

    @staticmethod
    def author_icon_url():
        icon_url = ""
        if settings.AUTHOR_ICON_URL is not "":
            icon_url = settings.AUTHOR_ICON_URL
        else:
            icon_url = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/6840712/Alliance_auth.png?width=64"

        return icon_url

    @staticmethod
    def prestring(state=False):
        if not state:
            pre_string = ""
        else:
            pre_string = "This is a Pre-Ping for an upcoming event.\n"
        return pre_string

    def get_color(self, state=False, hook_type='all'):
        color = Color.objects.filter(hook_type=hook_type, preping=state)
        if len(color) > 0:
            color = color[0].color[1:]  # Remove the '#'
            int_color = int(color, 16)  # Convert from Hex to Int.
            return int_color
        else:
            # All is the fallback color. If all is not set, then use a yellow color.
            if hook_type is 'all':
                return 16776960
            else:
                return self.get_color(state, 'all')




@shared_task()
def check_timerboard_models(preping=False, hours=0, minutes=59, seconds=59):
    if 'allianceauth.timerboard' in settings.INSTALLED_APPS:
        from allianceauth.timerboard.models import Timer
    else:
        logger.warning("Timerboard module not found. Skipped timerboard model check."
                       "Scheduling this task without having the timerboard module installed is not recommended.")
        return

    now = datetime.utcnow()
    next_hour = now + timedelta(hours=hours, minutes=minutes, seconds=seconds)

    timers = Timer.objects.filter(eve_time__range=(now, next_hour))

    for timer in timers:
        send_timer_event.apply_async((timer.pk, preping), eta=timer.eve_time)
        if preping:
            send_timer_event.apply_async((timer.pk, preping), eta=timer.eve_time - timedelta(minutes=30))

    return


@shared_task()
def send_timer_event(timer_id, preping=False, title_post="", desc_post=""):
    # Embed Settings Object
    sets = EmbedSettings()

    # If this task is created, then we know that the timerboard module is in fact installed.
    from allianceauth.timerboard.models import Timer
    timer = Timer.objects.get(pk=timer_id)

    hook_types = ['allianceauth.timerboard', 'all']

    hooks = Webhook.objects.filter(hook_type__in=hook_types)

    # Build Payload
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
    }

    payload = {
        "embeds": [{
            "title": "{} Timer in {} {}".format(timer.structure, timer.system, title_post),
            "description": "{}Details: {}\n{}".format(sets.prestring(preping), timer.details, desc_post),
            "url": sets.full_url(reverse('timerboard:view')),
            "color": 16776960,
            "timestamp": datetime.strftime(timer.eve_time, "%Y-%m-%dT%H:%M:%SZ"),
            "author": {
                "name": settings.AUTHOR_NAME,
                "url": sets.full_url('/'),
                "icon_url": sets.author_icon_url(),
            },
            "fields": [
                {
                    "name": "Structure",
                    "value": timer.structure,
                    "inline": True,
                },
                {
                    "name": "System",
                    "value": timer.system,
                    "inline": True,
                },
                {
                    "name": "Objective",
                    "value": timer.objective,
                }
            ]
        }]
    }

    # Send Payload
    for hook in hooks:
        url = hook.hook
        payload['embeds'][0]['color'] = sets.get_color(preping, hook.hook_type)
        r = requests.post(url, headers=headers, json=payload)

    logger.critical("Timer event happened! {}".format(timer.details))
    return


@shared_task()
def check_optimer_models(preping=False, hours=0, minutes=59, seconds=59):
    if 'allianceauth.optimer' in settings.INSTALLED_APPS:
        from allianceauth.optimer.models import OpTimer
    else:
        logger.warning("Optimer module not found. Skipped optimer model check."
                       "Scheduling this task without having the optimer module installed is not recommended.")
        return

    now = datetime.utcnow()
    next_hour = now + timedelta(hours=hours, minutes=minutes, seconds=seconds)

    timers = OpTimer.objects.filter(start__range=(now, next_hour))
    for timer in timers:
        send_optimer_event.apply_async((timer.pk, preping), eta=timer.start)
        if preping:
            send_optimer_event.apply_async((timer.pk, preping), eta=timer.start - timedelta(minutes=30))

    return


@shared_task()
def send_optimer_event(timer_id, preping=False, title_post="", desc_post=""):
    # Embed Settings Object
    sets = EmbedSettings()

    # If this task is created, then we know that the timerboard module is in fact installed.
    from allianceauth.optimer.models import OpTimer
    timer = OpTimer.objects.get(pk=timer_id)

    hook_types = ['allianceauth.optimer', 'all']

    hooks = Webhook.objects.filter(hook_type__in=hook_types)

    # Build Payload
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
    }

    payload = {
        "embeds": [{
            "title": "{} Fleet Forming in {} {}".format(timer.doctrine, timer.system, title_post),
            "description": "{}Description: {}\n{}".format(sets.prestring(preping), timer.operation_name, desc_post),
            "url": sets.full_url(reverse('optimer:view')),
            "color": 16776960,
            "timestamp": datetime.strftime(timer.start, "%Y-%m-%dT%H:%M:%SZ"),
            "author": {
                "name": settings.AUTHOR_NAME,
                "url": sets.full_url('/'),
                "icon_url": sets.author_icon_url(),
            },
            "fields": [
                {
                    "name": "Fleet Commander",
                    "value": timer.fc,
                    "inline": True,
                },
                {
                    "name": "System",
                    "value": timer.system,
                    "inline": True,
                },
                {
                    "name": "Doctrine",
                    "value": timer.doctrine,
                    "inline": True,
                },
                {
                    "name": "Duration",
                    "value": timer.duration,
                    "inline": True,
                }
            ]
        }]
    }

    # Send Payload
    for hook in hooks:
        url = hook.hook
        r = requests.post(url, headers=headers, json=payload)

    logger.critical("OpTimer event happened! {}".format(timer.operation_name))
    return


@shared_task()
def check_moonstuff_models(preping=False, hours=0, minutes=59, seconds=59):
    if 'moonstuff' in settings.INSTALLED_APPS:
        from moonstuff.models import ExtractEvent
    else:
        logger.warning("Optimer module not found. Skipped optimer model check."
                       "Scheduling this task without having the optimer module installed is not recommended.")
        return

    now = datetime.utcnow()
    next_hour = now + timedelta(hours=hours, minutes=minutes, seconds=seconds)

    events = ExtractEvent.objects.filter(arrival_time__range=(now, next_hour))
    for event in events:
        send_extract_event.apply_async((event.pk, preping), eta=event.arrival_time)
        if preping:
            send_extract_event.apply_async((event.pk, preping), eta=event.arrival_time - timedelta(minutes=30))

    return


@shared_task
def send_extract_event(event_id, preping=False, title_post="", desc_post=""):
    # Embed Settings Object
    sets = EmbedSettings()

    # If this task is created, then we know that the timerboard module is in fact installed.
    from moonstuff.models import ExtractEvent
    event = ExtractEvent.objects.get(pk=event_id)

    hook_types = ['moonstuff.extraction', 'all']

    hooks = Webhook.objects.filter(hook_type__in=hook_types)

    # Internal Function to get moon resources
    def _moon_resources(moon):
        resources = []
        for resource in moon.resources.all():
            resources.append(resource.ore)
        return resources

    if len(_moon_resources(event.moon)) == 0:
        resources = "No Resource data for this moon yet. Check the moon section of auth later for resource details."
    else:
        resources = "**\n**".join(_moon_resources(event.moon))

    # Build Payload
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
    }

    payload = {
        "embeds": [{
            "title": "{} Extraction Finishing at {} {}".format(event.moon.name, event.arrival_time, title_post),
            "description": "{}Description: Contains the following ores:\n\n**{}**".format(sets.prestring(preping),
                                                                                          resources
                                                                                          ),
            "url": sets.full_url(reverse('moonstuff:moon_info', args=[event.moon.moon_id])),
            "color": 16776960,
            "timestamp": datetime.strftime(event.arrival_time, "%Y-%m-%dT%H:%M:%SZ"),
            "author": {
                "name": settings.AUTHOR_NAME,
                "url": sets.full_url('/'),
                "icon_url": sets.author_icon_url(),
            },
            "fields": [
                {
                    "name": "Refinery",
                    "value": event.structure.name,
                    "inline": True,
                },
                {
                    "name": "Corporation",
                    "value": event.corp.corporation_name,
                    "inline": True,
                },
            ]
        }]
    }

    # Send Payload
    for hook in hooks:
        url = hook.hook
        r = requests.post(url, headers=headers, json=payload)

    logger.critical("Timer event happened! {}".format(event.moon.name))
    return
